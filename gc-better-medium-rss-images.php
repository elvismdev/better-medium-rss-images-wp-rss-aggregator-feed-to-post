<?php
/**
 * Plugin Name: GC Better Medium RSS Images (WP RSS Aggregator - Feed to Post)
 * Plugin URI: https://bitbucket.org/grantcardone/gc-better-medium-rss-images-wp-rss-aggregator-feed-to-post
 * Description: Filter to change the images sizes to import from the Medium RSS feed. This plugin/filter aims to be used in companion with the Feed to Post addon from the WP RSS Aggregator plugin.
 * Version: 1.0
 * Author: Elvis Morales
 * Author URI: https://twitter.com/n3rdh4ck3r
 * Requires at least: 3.5
 * Tested up to: 4.3
 */
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

function gc_better_medium_rss_images( $content ) {

	if ( stripos( $content, 'fit/c/600/200' ) === FALSE ) {
		return $content;
	}

	$content = str_replace( 'fit/c/600/200', 'max/500', $content ); // Replace the default fixed cropped size of 600 x 200 for the images which gives the RSS feed source by the full image with a maximum width of 500px. Height is automatically set based on image aspect ratio.

	return $content;

}
add_filter( 'wprss_ftp_converter_post_content', 'gc_better_medium_rss_images', 10, 1 );
